from decimal import Decimal

import pytest
from accept_types import AcceptableType, get_best_match, parse_header


def test_acceptable_type_full():
    """Test AcceptableType with a literal 'image/png' mime type."""
    a = AcceptableType('image/png;q=1.23;foo=bar')
    assert a.mime_type == 'image/png'
    assert a.weight == Decimal('1.23')
    assert a.matches('image/png')
    assert not a.matches('image/jpeg')
    assert not a.matches('text/html')


def test_acceptable_type_wild_minor():
    """Test AcceptableType with 'image/*' minor wildcards."""
    a = AcceptableType('image/*;foo=bar;q=0.9')
    assert a.mime_type == 'image/*'
    assert a.weight == Decimal('0.9')
    assert a.matches('image/png')
    assert a.matches('image/jpeg')
    assert not a.matches('text/html')


def test_acceptable_type_wild():
    """Test AcceptableType with '*' wildcards."""
    a = AcceptableType('*;foo=bar;q=0.3')
    assert a.mime_type == '*'
    assert a.weight == Decimal('0.3')
    assert a.matches('image/png')
    assert a.matches('text/html')


def test_acceptable_type_minor_plus():
    """Check that AcceptableType behaves with 'application/xhtml+xml'."""
    a = AcceptableType('application/xhtml+xml;q=0.1')
    assert a.mime_type == 'application/xhtml+xml'
    assert a.weight == Decimal('0.1')
    assert a.matches('application/xhtml+xml')
    assert not a.matches('application/xhtmllxml')
    assert not a.matches('application/xhtmxml')
    assert not a.matches('text/html')


def test_acceptable_type_invalid():
    with pytest.raises(ValueError):
        AcceptableType('nope')

def test_acceptable_type_no_q():
    a = AcceptableType('image/png')
    assert a.mime_type == 'image/png'
    assert a.weight == 1


def test_acceptable_type_bad_q():
    a = AcceptableType('image/png;q=nope')
    assert a.mime_type == 'image/png'
    assert a.weight == 1


def test_get_best_match():
    assert get_best_match('image/png,image/*;q=0.9,*;q=0.3', ['text/html', 'image/png']) \
        == 'image/png'
    assert get_best_match('image/png,image/*;q=0.9,*;q=0.3', ['text/html', 'image/jpeg']) \
        == 'image/jpeg'
    assert get_best_match('image/png,image/*;q=0.9,*;q=0.3', ['image/png', 'image/jpeg']) \
        == 'image/png'
    assert get_best_match('image/png,image/*;q=0.9,*;q=0.3', ['text/html']) \
        == 'text/html'
    assert get_best_match('image/png,application/json', ['text/html', 'image/jpeg']) \
        == None


def test_parse_header():
    """Test that :func:`parse_header` parses and sorts things."""
    assert parse_header('image/*;q=0.9,image/png,*;q=0.3,application/xhtml+xml;q=0.5') == [
        AcceptableType('image/png'),
        AcceptableType('image/*;q=0.9'),
        AcceptableType('application/xhtml+xml;q=0.5'),
        AcceptableType('*;q=0.3'),
    ]
